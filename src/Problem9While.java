import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        int i = 0;
        Scanner n = new Scanner(System.in);
        System.out.print("Please input n: ");
        int num = n.nextInt();
        while (i < num) {
            int j = 1;
            while (j <= num) {
                System.out.print(j+"");
                j++;
            }
            i++;
            System.out.println();
        }
    }
}
