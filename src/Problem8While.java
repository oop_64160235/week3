public class Problem8While {
    public static void main(String[] args) {
        int i = 0;
        while (i < 5) {
            int n = 1;
            while (n < 6) {
                System.out.print(n);
                n++;
            }
            i++;
            System.out.println();
        }
    }
}
