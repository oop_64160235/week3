import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        int n, num;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.print("Please select star type [1-4],5 is Exit]:");
            n = sc.nextInt();
            
            if (n < 5) {
                if (n == 1) {
                    System.out.print("Please input a number: ");
                    num = sc.nextInt();
                    for (int i = 1; i <= num; i++) {
                        for (int j = 1; j <= i; j++) {
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                } else if (n == 2) {
                    System.out.print("Please input a number: ");
                    num = sc.nextInt();
                    for (int i = num; i >= 0; i--) {
                        for (int j = 1; j < i + 1; j++) {
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                } else if (n == 3) {
                    System.out.print("Please input a number: ");
                    num = sc.nextInt();
                    for (int i = 0; i < num; i++) {
                        for (int j = 0; j < i; j++) {
                            System.out.print(" ");
                        }
                        for (int j = 1; j < num - i + 1; j++) {
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                } else if (n == 4) {
                    System.out.print("Please input a number: ");
                    num = sc.nextInt();
                    for (int i = 5; i >= 0; i--) {
                        for (int j = 1; j <= num; j++) {
                            if (j >= i + 1) {
                                System.out.print("*");
                            } else {
                                System.out.print(" ");
                            }
                        }
                        System.out.println();
                    }
                }
            } else if (n == 5) {
                System.out.println("Bye bye!!!");
                System.exit(0);
            } else {
                System.out.println("Error: Please input number between 1-5");
            }
        } while (n != 5);
    }
}