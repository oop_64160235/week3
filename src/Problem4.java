import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner n = new Scanner(System.in);
        int number;
        int sum = 0;
        int count = 0;
        do {
            System.out.print("Please input number: ");
            number = n.nextInt();
            if (number == 0) {
                break;
            }
            sum = sum + number;
            count++;
            System.out.println("Sum: " + sum + "," + "Avg: " + (((double) sum) / count));
        } while (number != 0);
        System.out.println("Bye");
    }
}
